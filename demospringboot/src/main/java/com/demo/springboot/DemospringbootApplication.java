package com.demo.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class DemospringbootApplication {


	public static void main(String[] args) {
	SpringApplication.run(DemospringbootApplication.class, args);
	}

}
