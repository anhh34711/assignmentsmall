package com.demo.springboot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.springboot.model.Help;
import com.demo.springboot.model.Staff;
import com.demo.springboot.reponsitory.HelpReponsitory;
import com.demo.springboot.reponsitory.StaffReponsitory;
import com.demo.springboot.service.Helpservice;
import com.demo.springboot.service.Staffservice;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class Controller {
	@Autowired
	StaffReponsitory staffReponsitory;
	@Autowired
	HelpReponsitory helpReponsitory;
	@Autowired
	Helpservice helpservice;
	@Autowired
	Staffservice staffservice;

	@GetMapping("searchStaff")
	public ResponseEntity<?> searchByStatus(@RequestParam String status, @RequestBody Staff staff) {
		List<Staff> listStaff = staffservice.searchStaff(status, staff);
		return ResponseEntity.ok(listStaff);
	}

	@GetMapping("searchHelp/{staffId}")
	public ResponseEntity<?> searchHelpByStaffId(@PathVariable String staffId, @RequestBody Help help) {
		List<Help> listHelp = helpservice.searchHelpByStaffId(help, staffId);
		if (listHelp == null) {
			ResponseEntity.status(HttpStatus.NO_CONTENT).body("no data found!");
		}
		return ResponseEntity.ok(listHelp);
	}

	@GetMapping("addHelp")
	public ResponseEntity<?> addHelp(@RequestBody Help help) {
		Help newHelp=helpservice.createHelp(help);
		return ResponseEntity.ok(newHelp);
	}

	@Transactional(rollbackFor = Exception.class)
	@GetMapping("update/{id}")
	public ResponseEntity<?> updateHelp(@RequestBody Help help, @PathVariable String id) {
		Help resultHelp =helpservice.updateHelp(help);
		return resultHelp==null?ResponseEntity.status(HttpStatus.BAD_REQUEST).body("no data found!"):ResponseEntity.ok(resultHelp);
	}

	@GetMapping("deleteHelp/{id}")
	public ResponseEntity<?> deleteHelp(@PathVariable String id) {
		boolean b=helpservice.deleteHelp(id);
		return b==true?ResponseEntity.ok("delete successful"):ResponseEntity.status(HttpStatus.BAD_REQUEST).body("delete failed");
	}
}
