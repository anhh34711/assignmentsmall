package com.demo.springboot.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
@Entity
@Data
public class Help {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long helpid;
	
	@Column(length = 50)
	private String helpname;
	
	@Column(length = 50)
	private String parenthelpid;
	
	@Column(length = 50)
	private String type;
	
	@Column(length = 50)
	private String position;
	
	@Column(length = 50)
	private String content;
	 
	@Column(length = 50)
	private String status;
	       
	@Column(length = 50)
	private String createduser;


	
}
