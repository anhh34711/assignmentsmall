package com.demo.springboot.model;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
@Entity
@Data
public class Shop {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long shopid;
	@Column(length = 50)
	private String shopcode;
	@Column(length = 50)
	private String shopname;
	@Column(length = 50)
	private String tel;
	@Column(length = 50)
	private String address;
	@Column(length = 50)
	private String shoppath;
	@Column(length = 50)
	private boolean status;
	@Column(length = 50)
	private Date createddate;
}
