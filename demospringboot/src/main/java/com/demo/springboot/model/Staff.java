package com.demo.springboot.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Staff {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long staffid;
	@Column(length = 50)
	private String staffcode;
	@Column(length = 50)
	private String staffname;
	@Column(length = 50)
	private String tel;
	@Column(length = 50)
	private String address;
	@Column(length = 50)
	private String idno;
	@Column
	private long shopid;
	@Column
	private boolean status;

}
