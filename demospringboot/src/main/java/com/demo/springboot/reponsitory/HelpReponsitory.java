package com.demo.springboot.reponsitory;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.demo.springboot.model.Help;

public interface HelpReponsitory extends JpaRepository<Help, Long> {
	// find help in status=1
	@Query(value = "select h.* from help h,staff s where h.createduser=s.staffcode and s.staffid=?1 and s.content=?3 and"
			+ " s.position=?4 and s.type=?5", nativeQuery = true)
	public List<Help> searchByStaff(int staffid);

	// delete help by unable status
//	@Query(value = "update help set status='0' where helpid=?1", nativeQuery = true)
//	public boolean UnableHelp(int id);
}
