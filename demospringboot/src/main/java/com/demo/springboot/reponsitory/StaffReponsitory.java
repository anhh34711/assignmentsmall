package com.demo.springboot.reponsitory;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.demo.springboot.model.Staff;

public interface StaffReponsitory extends JpaRepository<Staff, Long> {
	@Query(value = "select * from staff where staffcode=?1",nativeQuery = true)
    public Staff searchByCode(String code);
	@Query(value = "select * from staff where status=?1 and staffcode=?2",nativeQuery = true)
    public List<Staff> searchBystatus(int status,String code);
	@Query(value = "select * from staff where staffname like %?1% ",nativeQuery = true)
    public List<Staff> searchByName(String name);
	
	
}
