package com.demo.springboot.service;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.demo.springboot.model.Help;
import com.demo.springboot.reponsitory.HelpReponsitory;
import lombok.extern.slf4j.Slf4j;

@Service
@Transactional(rollbackFor = Exception.class)
@Slf4j
public class Helpservice {
	@Autowired
	HelpReponsitory helpReponsitory;
	@PersistenceContext
	EntityManager entityManager;
	
    // update if help.id exists else then no created or update
	public Help updateHelp(Help help) {
		if(help==null||help.getHelpid()==0||helpReponsitory.findById(help.getHelpid())==null) {
			return null;
		}
		helpReponsitory.saveAndFlush(help);
		return help;
	}

	public Help createHelp(Help help) {
		helpReponsitory.save(help);
		return help;
	}

	// delete help by set status = 0
	public boolean deleteHelp(String id) {
		if (id == null) {
			return false;
		}
		try {
			Help help = helpReponsitory.findById(Long.parseLong(id)).orElseThrow(null);
			if (help == null)
				throw new Exception();
			help.setStatus("0");
			helpReponsitory.save(help);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public List<Help> searchHelpByStaffId(Help help, String staffId) {
		try {
			if (help == null || staffId == null) {
				return null;
			}
			Query queryHelp = entityManager.createNativeQuery(
					"select h.* from help h,staff s where s.staffid=" + staffId
							+ (help.getContent() != null ? " and h.content='" + help.getContent() + "'" : "")
							+ (help.getCreateduser() != null ? " and h.createduser='" + help.getCreateduser() + "'"
									: "")
							+ (help.getHelpname() != null ? " and h.helpname='" + help.getHelpname() + "'" : "")
							+ (help.getPosition() != null ? " and h.position='" + help.getPosition() + "'" : "")
							+ (help.getStatus() != null ? " and h.status='" + help.getStatus() + "'" : "")
							+ (help.getType() != null ? " and h.type='" + help.getType() + "'" : "")
							+ (help.getParenthelpid() != null ? " and h.parenthelpid='" + help.getParenthelpid() + "'"
									: "")
							+ (help.getHelpid() != 0 ? " and h.helpid=" + help.getHelpid() : ""),
					Help.class);
			List<Help> listHelp = queryHelp.getResultList();
			log.info("query successful");

			return listHelp;
		} catch (Exception e) {
			log.error("happened error with message: ", e);
			return null;
		}
	}
}
