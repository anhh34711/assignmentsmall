package com.demo.springboot.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.demo.springboot.model.Staff;

@Service
//@Transactional(noRollbackFor = Exception.class)
public class Staffservice {
	@PersistenceContext
	EntityManager entityManager;

public List<Staff> searchStaff(String status,Staff staff){
	if(status==null||staff==null) {
		return null;
	}
	Query queryStaff=entityManager.createNamedQuery("select * from staff where status="+status+
	                                                   (staff.getAddress()!=null?" and address='"+staff.getAddress()+"'":"")+
	                                                   (staff.getIdno()!=null?" and idno='"+staff.getIdno()+"'":"")+
	                                                   (staff.getStaffcode()!=null?" and staffcode='"+staff.getStaffcode()+"'":"")+
	                                                   (staff.getStaffname()!=null?" and staffname='"+staff.getStaffname()+"'":"")+
	                                                   (staff.getTel()!=null?" and tel='"+staff.getTel()+"'":"")+
	                                                   (staff.getShopid()==0?" and shopid="+staff.getShopid():"")+
	                                                   (staff.getStaffid()==0?" and staffid="+staff.getStaffid():"")
	                                                   ,Staff.class);
	List<Staff> listStaff=queryStaff.getResultList();
	return listStaff;
}
}
