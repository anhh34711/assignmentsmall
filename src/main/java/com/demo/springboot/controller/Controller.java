package com.demo.springboot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.springboot.model.Help;
import com.demo.springboot.model.Staff;
import com.demo.springboot.reponsitory.HelpReponsitory;
import com.demo.springboot.reponsitory.StaffReponsitory;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class Controller {
	@Autowired
	StaffReponsitory staffReponsitory;
	@Autowired
	HelpReponsitory helpReponsitory;

	@GetMapping("searchstaff")
	public ResponseEntity<?> searchByStatus(@RequestParam String status, @RequestParam String code) {
		try {
			List<Staff> l=staffReponsitory.searchBystatus(Integer.parseInt(status), code);
			if(l.size()<1)
				 throw new Exception();
			return ResponseEntity.ok().body(l);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("nothing found");
		}
	}

	@GetMapping("searchhelp/{id}")
	public ResponseEntity<?> searchHelpByStaffId(@PathVariable String id, @RequestParam String name) {
		try {
			 List<Help> l=	helpReponsitory.searchByStaff(Integer.parseInt(id), name);
			 if(l.size()<1)
				 throw new Exception();
			return ResponseEntity.ok().body(l);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("nothing found");
		}
	}

	@GetMapping("addhelp")
	public ResponseEntity<?> AddHelp(@RequestBody Help h) {
		try {
			helpReponsitory.save(h);// nếu id tồn tại thì add else update
			return ResponseEntity.ok().body(h);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(null);
		}
	}

	@Transactional(rollbackFor = Exception.class)
	@GetMapping("update/{id}")
	public ResponseEntity<?> UpdateHelp(@RequestBody Help h, @PathVariable String id) {
		try {
			Help h1 = helpReponsitory.findById(Long.parseLong(id)).orElseThrow(null);
			if (h1 == null)
				throw new Exception();
			helpReponsitory.save(h1);
			return ResponseEntity.ok().body(h);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not found id");
		}
	}

	@GetMapping("deletehelp/{id}")
	public ResponseEntity<?> DeleteHelp(@PathVariable String id) {
		try {
			Help h = helpReponsitory.findById(Long.parseLong(id)).orElseThrow(() -> null);
			if (h == null)
				throw new Exception();
			h.setStatus("0");
			return ResponseEntity.ok().body(helpReponsitory.save(h));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not found id");
		}
	}
}
